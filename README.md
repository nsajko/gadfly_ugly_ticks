## Some plotting experiments with Gadfly.jl

The idea is to generate a number of random polynomials, plot them and then see what
fraction of the plots are ugly and in what way. In particular I'm interested in the way
Gadfly calculates the ticks.

The `data/reproduce.jl` Julia file and the plots in `data/generated_plots` were generated
by running `src/generated_polynomials.jl`

The script `src/hardcoded_polynomials.jl` uses `data/reproduce.jl` to reproduce the
plots.

### Some types of possible aesthetic issues

1. Left-right imbalance: when there's significantly more blank space on one side of the
   plot than on the other.

    a. absolute: like absolute numerical error: simply the distance (absolute difference)
       between the left blank space width and right blank space width

    b. relative: like relative numerical error: like above but with accounting for
       relative size differences. This an issue, in particular, when the plot has *no*
       blank space on one side, but there is some on the other side.

2. Huge amounts of blank space: I'd put the cut off at when more than about a third of
   the total available space is wasted/blank.

### Conclusion

Ratio of plots exhibiting issue 1: `27/50 = 0.54`

Ratio of plots exhibiting issue 2: `12/50 = 0.24`

### Subjective analysis of individual plots

I'll try to analyse the aesthetic qualities of each plot with regards to how the x-axis
ticks were chosen by Gadfly. This is based on visual inspection because of laziness, as
numeric analyis of the ticks would presumably require modifying the Gadfly
implementation.

1. OK

2. 1

3. OK

4. OK

5. 2

6. 1 2

7. OK

8. OK

9. 1

10. 1

11. 1 2

12. 1

13. 1

14. 1

15. 2

16. 1 2

17. 1 2

18. OK

19. 1

20. 1

21. OK

22. OK

23. 1

24. OK

25. OK

26. 2

27. 1

28. OK

29. OK

30. 1

31. 1 2

32. OK

33. 1

34. OK

35. OK

36. 1 2

37. 1

38. 1 2

39. 1

40. 1

41. 1

42. 2

43. 1

44. 1

45. 1

46. 1

47. OK

48. OK

49. 2

50. OK
