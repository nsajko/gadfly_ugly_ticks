# Copyright © 2023 Neven Sajko. All rights reserved.

module Perturbed

# Equivalent to `Int8(-1)^n` and `Int8(iseven(n) ? 1 : -1)`
pow_minus_one(n::Integer) =
  iseven(n)*Int8(2) - one(Int8)

function rand_uint_and_sign(::Type{T}) where {T <: Unsigned}
  local u = rand(widen(T))
  (
    pow_minus_one(iseven(u >> (8 * sizeof(T)))),
    T(u & ~zero(T)))
end

absolute_error(p::NTuple{2, <:Real}) = abs(first(p) - last(p))

function perturbed(prev::F, mid::F, next::F, factor::Real, ::Type{T}) where
{F <: Real, T <: Unsigned}
  local d = min(map(absolute_error, ((mid, prev), (mid, next)))...)
  local (s, n) = rand_uint_and_sign(T)
  mid + d * factor * s * (n * (one(F) / typemax(T)))
end

"""
Takes a sorted vector. Perturbs each non-extremal point. The bigger
the factor, the more perturbation there will be. Keep the factor
between zero and one to preserve order in the input.

Inspired by the perturbPoints function contained in Sollya's
remez.c file, except that this also works with rationals:

https://gitlab.inria.fr/sollya/sollya/-/blob/10f5b5551b58be4da58f077dd8cc5d01f2988172/remez.c#L1748-L1805
"""
function perturbed(v::AbstractVector{F}, factor::Real, ::Type{T} = UInt16) where
{F <: Real, T <: Unsigned}
  isempty(v) && (return F[])
  isone(length(v)) && (return F[F(first(v))])
  (length(v) == 2) && (return F[map(F, (first(v), last(v)))...])

  local ret = zeros(F, length(v))

  ret[begin] = F(v[begin])
  ret[end] = F(v[end])

  local neighb = (v, i) -> (v[i - 1], v[i], v[i + 1])

  # Keep order
  factor /= 2

  for i in 2:(length(v) - 1)
    ret[begin - 1 + i] = perturbed(neighb(v, firstindex(v) - 1 + i)..., factor, T)
  end

  ret
end

# A vector of `n` evenly-spaced and then randomly perturbed points.
function perturbed_grid_on_interval(
  n::Int,
  interval::NTuple{2, F},
  factor::Real,
  ::Type{T} = UInt16,
) where {
  F <: AbstractFloat,
  T <: Unsigned,
}
  m = n - 1
  a = F(interval[begin])
  b = F(interval[end])
  ab = b - a
  ret = Perturbed.perturbed(F[a + ab*(i // m) for i in 0:m], factor, T)
  ret[begin] = a
  ret[end] = b
  ret
end

end
