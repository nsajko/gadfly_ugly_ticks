# Copyright © 2023 Neven Sajko. All rights reserved.

module RandomRoots

import ..Perturbed

const Pe = Perturbed
const perturbed_on_interval = Pe.perturbed_grid_on_interval

struct RootCount
  count::Int

  function RootCount(rc::Int)
    (false ≤ rc) || error("negative root count")
    new(rc)
  end
end

Int(rc::RootCount) = rc.count

rand_uniform(lo::F, hi::F, n::Signed) where {F <: AbstractFloat} =
  (hi - lo) .* rand(F, n) .+ lo

function random_roots(
  root_counts::Vector{RootCount}, boundaries::Vector{F},
) where {F <: Real}
  len = length(root_counts)

  (length(boundaries) == len + 1) || error("mismatched argument lengths")

  roots = F[]

  for i ∈ eachindex(root_counts)
    itv = (boundaries[i], boundaries[i + 1])
    rc = root_counts[i]
    append!(roots, rand_uniform(itv..., Int(rc)))
  end

  roots
end

function areas(boundaries::V) where {V <: Vector{<:AbstractFloat}}
  a = V(undef, length(boundaries) - 1)
  for i ∈ 0:(length(a) - 1)
    a[begin + i] = boundaries[begin + i + 1] - boundaries[begin + i]
  end
  a
end

function filler_area(areas::Vector{<:AbstractFloat})
  f = zero(eltype(areas))
  for i ∈ 0:2:(length(areas) - 1)
    f += areas[begin + i]
  end
  f
end

function random_roots(
  root_counts::Vector{RootCount},
  filler_root_count::RootCount,
  lo::F,
  hi::F,
  ::Type{T} = Float64,
  perturb_factor::Real = 7//8,
) where {T <: Real, F <: Real}
  expanded_len = length(root_counts)*2 + 1
  boundaries = perturbed_on_interval(
    expanded_len + 1 + 2, (lo, hi), perturb_factor,
  )
  pop!(boundaries)
  popfirst!(boundaries)
  ars = areas(boundaries)
  total_filler = filler_area(ars)
  frc = Int(filler_root_count)

  expanded_rcs = Vector{RootCount}(undef, expanded_len)

  # copy original root counts
  for i ∈ 1:2:(expanded_len - 1)
    expanded_rcs[begin + i] = root_counts[begin + div(i, 2, RoundToZero)]
  end

  # filler root counts
  for i ∈ 0:2:expanded_len
    fc = ars[begin + i]/total_filler*frc
    fci = round(Int, fc, RoundToZero)
    expanded_rcs[begin + i] = RootCount(fci)
  end

  bo = map(T, boundaries)

  roots = random_roots(expanded_rcs, bo)

  (roots = roots, interval_boundaries = bo)
end

random_roots(rcs::Vector{Int}, frc::Int) = random_roots(
  map(RootCount, rcs), RootCount(frc), big"-3.0", big"3.0",
)

end
