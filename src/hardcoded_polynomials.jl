# Copyright © 2023 Neven Sajko. All rights reserved.

setprecision(BigFloat, 2000)

include("Plotting.jl")

const repro_data = (include("../data/reproduce.jl"))

function main()
  rootses = Vector{Float64}[]
  intervals = NTuple{2, Float64}[]
  for rts_itv ∈ repro_data
    rts = rts_itv.roots
    itv = rts_itv.interval
    push!(rootses, rts)
    push!(intervals, itv)
  end

  Plotting.write_polynomial_plots_from_roots(
    rootses, intervals, "/tmp/hardcoded_plots",
  )
end

main()
