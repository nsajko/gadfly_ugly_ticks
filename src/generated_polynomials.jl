# Copyright © 2023 Neven Sajko. All rights reserved.

setprecision(BigFloat, 2000)

include("Plotting.jl")
include("Perturbed.jl")
include("RandomRoots.jl")

function boundaries_to_intervals(b::Vector{F}) where {F <: Real}
  t = b[(begin + 1):(end - 1)]
  ret = NTuple{2, F}[]
  for i ∈ eachindex(t)[begin:2:end]
    push!(ret, (t[i], t[i + 1]))
  end
  ret
end

const plot_count = 50

function print_repro(rootses, intervals)
  println("[")
  for rts_itv ∈ zip(rootses, intervals)
    rts = first(rts_itv)
    itv = last(rts_itv)
    print(
      "  (\n",
      "    roots = ", rts, ",\n",
      "    interval = ", itv, ",\n",
      "  ),\n",
    )
  end
  println("]")
end

function main()
  rootses = Vector{Float64}[]
  intervals = NTuple{2, Float64}[]
  for i ∈ 1:plot_count
    rts_boundaries = RandomRoots.random_roots([0], 10)
    rts = rts_boundaries.roots
    bnds = rts_boundaries.interval_boundaries
    itv = only(boundaries_to_intervals(bnds))
    push!(rootses, rts)
    push!(intervals, itv)
  end

  print_repro(rootses, intervals)

  Plotting.write_polynomial_plots_from_roots(
    rootses, intervals, "/tmp/generated_plots",
  )
end

main()
