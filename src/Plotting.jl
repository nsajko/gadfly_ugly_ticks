# Copyright © 2023 Neven Sajko. All rights reserved.

module Plotting

using Gadfly
import Polynomials

plot_val(fun::Fun, itv::NTuple{2, <:Real}) where {Fun <: Function} =
  plot(
    y = [Float64 ∘ fun],
    xmin = [Float64(first(itv))],
    xmax = [Float64(last(itv))],
    Stat.func(num_samples = 2500), Geom.line(),
    Guide.xlabel(nothing), Guide.xticks(label = true),
    Guide.ylabel(nothing), Guide.yticks(label = true),
  )

svg_file(file::String) = SVG(file, 16cm, 12cm)

write_plot(p, file::String) = draw(svg_file(file), p)

write_plot(
  fun::Fun, itv::NTuple{2, <:Real}, file::String,
) where {Fun <: Function} =
  write_plot(plot_val(fun, itv), file)

function write_polynomial_plot_from_roots(
  roots::AbstractVector{<:Real}, itv::NTuple{2, <:Real}, file::String,
)
  polynomial = Polynomials.fromroots(map(BigFloat, roots))
  func_to_plot = let p = polynomial
    (x::Real) -> evalpoly(x, p)
  end
  write_plot(func_to_plot, itv, file)
end

function write_polynomial_plots_from_roots(
  rootses, itvs, dir::String,
)
  for i ∈ eachindex(itvs)
    write_polynomial_plot_from_roots(
      rootses[i], itvs[i], string(dir, "/", i, ".svg"),
    )
  end
  nothing
end

end
